# Bash Helpers

This project contains a series of scripts that provide useful functionality for bash scripts.

## Conventions

To avoid naming conflicts, these helpers use the following conventions

 * All global variables are prefixed with **FOO_**

 * All functions are prefixed with **foo_**

## Helpers

 * colors.sh - Provides an easy way to add color to your shell output 
