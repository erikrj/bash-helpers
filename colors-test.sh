#!/usr/bin/env bash

###############################################################################
# Tests for colors.sh
# See https://bitbucket.org/erikrj/bash-helpers/src/master/
###############################################################################

set -o pipefail && set -ue

source colors.sh

echo -e "$(cred)red$(creset)"
echo -e "$(foo_colors_red)red$(foo_colors_reset)"
echo -e "$(cred)red$(foo_colors_reset)"
echo -e "$(cblue)blue$(creset)"
foo_colors_print_all
