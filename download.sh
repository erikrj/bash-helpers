#!/usr/bin/env bash

###############################################################################
# Downloads all helpers into the current working directory
#
# This script can be executed remotely by running
#
# bash <(curl -s "https://bitbucket.org/erikrj/bash-helpers/raw/master/download.sh")
###############################################################################
set -o pipefail && set -uex
curl -sO "https://bitbucket.org/erikrj/bash-helpers/raw/master/colors.sh"
curl -sO "https://bitbucket.org/erikrj/bash-helpers/raw/master/logger.sh"
