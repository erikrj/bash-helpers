#!/usr/bin/env bash

###############################################################################
# Tests for logger.sh
# See https://bitbucket.org/erikrj/bash-helpers/src/master/
###############################################################################

set -o pipefail && set -ue

function line() {
  printf '\x2D%.0s' {1..80} && printf "\n"
}

source logger.sh

line && info "Testing default log level" && line
foo_logger_trace "message"
foo_logger_debug "message"
foo_logger_info "message"
foo_logger_warn "message"
foo_logger_error "message"
echo

foo_logger_enable_trace
line && info "Changed log level to trace" && line
foo_logger_trace "message"
foo_logger_debug "message"
foo_logger_info "message"
foo_logger_warn "message"
foo_logger_error "message"
echo

line && info "Testing shorntame" && line
trace "message"
debug "message"
info "message"
warn "message"
error "message"
echo

export FOO_LOGGER_DISPLAY_BASENAME=0
line && info "Testing no basname" && line
trace "message"
debug "message"
info "message"
warn "message"
error "message"
echo
