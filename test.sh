#!/usr/bin/env bash

function line() {
  printf '#%.0s' {1..80} && printf "\n"
}

for file in *-test.sh; do
  line && echo "Testing ${file}" && line
  "./${file}"
  echo ""
done

line && echo "Testing download.sh" && line
rm -rf downloads && mkdir downloads && cd downloads || exit 1
../download.sh
cd .. || exit 1
rm -rf downloads
